<?php

namespace Drupal\form_delegate\Exception;

/**
 * Class EntityFormModesNotSupportedException
 *
 * @package Drupal\form_delegate\Exception
 */
class EntityFormModesNotSupportedException extends \Exception {}
